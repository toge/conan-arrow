from conans import ConanFile, CMake, tools

class ArrowConan(ConanFile):
    name            = "arrow"
    version         = "1.0.0"
    license         = "Apache-2.0"
    url             = "https://arrow.apache.org/"
    description     = "Apache arrow (forked from https://github.com/FiligreeTech/conan-arrow)"
    topics          = ("apache", "arrow", "json", "csv", "parquet")
    settings        = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False]}
    default_options = "shared=False"
    generators      = "cmake"
    requires        = [
        "boost/[>= 1.68.0]@conan/stable",
        "lz4/[>= 1.8.0]@bincrafters/stable",
        "snappy/1.1.7@bincrafters/stable",
        "zlib/[>= 1.2.8]@conan/stable",
        "zstd/[>= 1.3.3]@bincrafters/stable",
        "rapidjson/1.1.0@bincrafters/stable",
        "bzip2/1.0.8@conan/stable",
        "thrift/[>= 0.13.0]@bincrafters/stable",
        "double-conversion/[>= 3.1.4]",
        "utf8proc/2.5.0",
    ]
    build_requires = [
        "thrift_installer/0.13.0@bincrafters/stable"
    ]

    def source(self):
        self.run("git clone https://github.com/apache/arrow.git")
        self.run("cd arrow && git checkout apache-arrow-{}".format(self.version))

        tools.replace_in_file("arrow/cpp/CMakeLists.txt", 'project(arrow VERSION "${ARROW_BASE_VERSION}")',
                              '''project(arrow VERSION "${ARROW_BASE_VERSION}")
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')
        if self.settings.os == "Windows":
            tools.replace_in_file("arrow/cpp/cmake_modules/ThirdpartyToolchain.cmake", "set(Boost_USE_STATIC_LIBS ON)", "set(Boost_USE_STATIC_LIBS OFF)")

    def configure_cmake(self):
        generator = "Ninja" if self.settings.os == "Windows" else None
        cmake = CMake(self, generator=generator)
        cmake.vebose = True
        cmake.definitions["ARROW_DEPENDENCY_SOURCE"] = "SYSTEM"
        cmake.definitions["ARROW_JEMALLOC"]         = "OFF"
        cmake.definitions["ARROW_BUILD_STATIC"]     = "ON"
        cmake.definitions["ARROW_BUILD_SHARED"]     = "OFF"
        cmake.definitions["ARROW_BUILD_TESTS"]      = "OFF"
        cmake.definitions["ARROW_BOOST_USE_SHARED"] = "OFF"
        cmake.definitions["ARROW_COMPUTE"]          = "ON"
        cmake.definitions["ARROW_IPC"]              = "OFF"
        cmake.definitions["ARROW_HDFS"]             = "OFF"
        cmake.definitions["ARROW_PARQUET"]          = "ON"
        cmake.definitions["ARROW_BUILD_UTILITIES"]  = "OFF"
        cmake.definitions["ARROW_WITH_BROTLI"]      = "OFF"
        cmake.definitions["ARROW_WITH_LZ4"]         = "ON"
        cmake.definitions["ARROW_WITH_SNAPPY"]      = "ON"
        cmake.definitions["ARROW_WITH_ZLIB"]        = "OFF"
        cmake.definitions["ARROW_WITH_BZ2"]         = "ON"
        cmake.definitions["ARROW_WITH_ZSTD"]        = "ON"
        cmake.definitions["ARROW_USE_GLOG"]         = "OFF"
        
        if self.settings.os == "Windows":
            cmake.definitions["CMAKE_BUILD_TYPE"]=str(self.settings.build_type)

        cmake.configure(source_folder="arrow/cpp")
        return cmake

    def build(self):
        self.configure_cmake().build()

    def package(self):
        self.configure_cmake().install()

    def package_info(self):
        if self.settings.os == "Windows":
            self.cpp_info.libs = ["arrow_static", "parquet"]
        else:
            self.cpp_info.libs = ["arrow", "parquet"]
