#include <iostream>
#include <memory>

#include "arrow/api.h"

int main() {
    constexpr int64_t buffer_size = 4096;

    auto pool = arrow::default_memory_pool();
    auto status = arrow::AllocateBuffer(buffer_size, pool);

    return 0;
}
